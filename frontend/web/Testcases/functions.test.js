const functions = require('./functions');
const {generateText} = require('./util');
const {validateInput} = require('./util');

test('Adds 2 + 2 = 4', ()=>{
	expect(functions.add(2, 2)).toBe(4);
});


//email validation
test('Email should contain @gmail.com', () => {
	email = ['@gmail.com'];
	expect(email).toContain('@gmail.com');
});

test('Should output exams start date and end date',() => {
	
	const text = generateText('Start of exams','20-07-2019' );
	expect(text).toBe('Start of exams (20-07-2019)');

});

test('Should output sticky note for end of exam period',() => {

	const text1 = generateText('End of exams','30-07-2019' );
	expect(text1).toBe('End of exams (30-07-2019)');
});

test('Should output a number i.e 23',() => {

	const text2 = validateInput('sang','',23 );
	expect(text2).toBe(true);
});