<?php ?>
<html>

<head>
   
   <title>TUK - Sticky Notes!</title>

   <!-- CSS -->
   <link rel="stylesheet" href="assets/css/main.css?version=1" />
   <link rel="stylesheet" href="assets/css/tuksticky.css?version=1" />

   <link href='http://fonts.googleapis.com/css?family=Architects+Daughter' rel='stylesheet' />
    
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
   <script>!window.jQuery && document.write(unescape('%3Cscript src="assets/js/jquery1.6.2.js"%3E%3C/script%3E'))</script>

   <script src="assets/js/respond.min.js"></script>
   <script src="assets/js/modernizr.custom.23610.js"></script>
   <script src="assets/js/tuksticky.js"></script>
   <script src="assets/js/prettyDate.js"></script>
</head>

<body>

	<!-- First header has an ID so you can give it individual styles, and target stuff inside it -->
	<header id="head">

      <div class="left topsection">
         <a href="#" id="addnote" class="tooltip blue-tooltip"><img src="assets/img/add.png" alt="Add a new sticky note"><span>Add a new sticky note</span></a>
         <a href="#" id="removenotes" class="tooltip blue-tooltip"><img src="assets/img/remove.png" alt="Remove all sticky notes"><span>Remove all sticky notes</span></a>
      </div>

      <div class="left topsection">
         <a href="#" id="shrink" class="tooltip blue-tooltip"><img src="assets/img/decrease.png" alt="Shrink"><span>Shrink sticky notes</span></a>
         <a href="#" id="expand" class="tooltip blue-tooltip"><img src="assets/img/increase.png" alt="Expand"><span>Expand sticky notes</span></a>
      </div>

	</header><!-- #head -->

   <div id="main"></div>

   <div class="clear">&nbsp;</div>
   <div class="clear">&nbsp;</div>

      <script>
        (function() {
          var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
          po.src = 'https://apis.google.com/js/plusone.js';
          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
        })();
      </script>      
   </footer>

</body>
</html>