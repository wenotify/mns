<?php 

/**
 * 
 */
class UserTest extends \PHPUnit_Framework_TestCase
{
	
	public function testThatWeCanGetTheFirstname()
	{
		$user = new \App\models\user;

		$user-> setFirstname('Frank');

		$this->assertEquals($user->getFirstname(), 'Frank');
	}

	public function testThatWeCanGetTheLastname()
	{
		$user = new \App\models\user;

		$user-> setLastname('Kip');

		$this->assertEquals($user->getLastname(), 'Kip');
	}

	public function testFullname()
	{
		$user = new \App\models\user;
		$user-> setFirstname('Frank');
		$user-> setLastname('Kip');

		$this->assertEquals($user->getFullname(), 'Frank Kip');
	}
}