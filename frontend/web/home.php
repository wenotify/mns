<?php 
	include 'header.php';
 ?>
<body>

	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8 desc">
			<h2>Mass Notification System</h2>
		<p>
			This is a Notification System <br>
			for sending bulk SMS and E-mail notifications<br>
			to students<br>
			and staff<br>
			in The Technical University of Kenya<br>
			
		</p>

		<a style="margin-left: 20%;" href="sms.php"><button class="btn btn-info">Send sms</button></a>
		<a href="send_email.php"><button class="btn btn-primary">Send E-mail</button></a>
		<br><br>
		</div>
		<div class="col-sm-2"></div>
		
		
	</div>
<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/6.3.3/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#config-web-app -->

<script>
  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyBrQBoc7UIQIHCYhXmGdI9MyG63dp8lwkI",
    authDomain: "massnotification-a3b16.firebaseapp.com",
    databaseURL: "https://massnotification-a3b16.firebaseio.com",
    projectId: "massnotification-a3b16",
    storageBucket: "",
    messagingSenderId: "641980752250",
    appId: "1:641980752250:web:f7e38763906f1429"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
</script>
</body>
<?php 
	include 'footer.php';
?> 